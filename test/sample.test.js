const { sum } = require('../src/sample')

describe('sum method', () => {
  it('should be able to ask for sum', () => {
    expect(sum(1, 2)).toBe(3)
  })

  it('should throw error if at least either of the arguments is null or empty', () => {
    expect(() => sum(null, 1)).toThrow()    
    expect(() => sum(undefined, 1)).toThrow()    
    expect(() => sum(1, null)).toThrow()    
    expect(() => sum(1, undefined)).toThrow()    
    expect(() => sum(null, undefined)).toThrow()    
  })
})