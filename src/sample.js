module.exports.sum = function (a, b) {
  if (a === undefined || a === null || b === undefined || b === null) throw new Error('args must not be null or undefined')
  return a + b
}
